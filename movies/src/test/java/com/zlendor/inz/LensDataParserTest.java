package com.zlendor.inz;

import com.zlendor.inz.movies.LensDataParser;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LensDataParserTest {
	private final LensDataParser parser = new LensDataParser();

	@Test
	public void parseImdbMovieRow_withQuotations() {
		final String[] parsed = parser.parseImdbMovieRow("11,\"American President, The (1995)\",Comedy|Drama|Romance");
		assertThat(parsed).containsExactly("11", "American President, The (1995)", "Comedy|Drama|Romance");
	}

	@Test
	public void parseImdbMovieRow_noQuotations() {
		final String[] parsed = parser.parseImdbMovieRow("9,Sudden Death (1995),Action");
		assertThat(parsed).containsExactly("9", "Sudden Death (1995)", "Action");
	}
}
