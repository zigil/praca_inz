package com.zlendor.inz.big;

import com.google.common.base.Optional;
import com.zlendor.inz.movies.ComparisonResult;
import com.zlendor.inz.movies.Summary;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.IOException;

public class MoviesBigSpark {
    public static void main(final String[] args) throws IOException {
        if (args.length < 3) {
            throw new RuntimeException("require 4 args: left right out");
        }

        final String comparisonOut = args[2]+"/comparison";
        final String summaryOut = args[2]+"/summary";

        final SparkConf conf = new SparkConf()
                .setAppName("Compare spark - test");

        final JavaSparkContext sc = new JavaSparkContext(conf);

        // [title -> [avg, votes]]
        final JavaPairRDD<String, Double[]> left = sc.textFile(args[0])
                .map(row -> row.trim().split("\t"))
                .mapToPair(row -> new Tuple2<>(
                        row[0],
                        new Double[]{
                                Double.valueOf(row[2]),
                                Double.valueOf(row[3])}
                ));

        // [title -> [avg, votes]]
        final JavaPairRDD<String, Double[]> right = sc.textFile(args[1])
                .map(row -> row.trim().split("\t"))
                .mapToPair(row -> new Tuple2<>(
                        row[0],
                        new Double[]{
                                Double.valueOf(row[2]),
                                Double.valueOf(row[3])}
                ));

        final JavaPairRDD<String, Tuple2<Optional<Double[]>, Optional<Double[]>>> joinedRdd = left
                .<Double[]>fullOuterJoin(right);

        final JavaPairRDD<String, ComparisonResult> comparisonRdd = joinedRdd
                .mapValues(records -> compare(records))
                .cache();

        comparisonRdd
                .mapValues(result -> result.toString())
                .saveAsTextFile(comparisonOut);

        Summary summary = comparisonRdd.aggregate(
                new Summary(),
                (s, r) -> s.add(r._2().getComparison()),
                (s1, s2) -> s1.add(s2));

        System.out.println(summary.toString());
    }

    private static ComparisonResult compare(final Tuple2<Optional<Double[]>, Optional<Double[]>> records) {
        return new ComparisonResult(records._1().orNull(), records._2().orNull()).invoke();
    }
}
