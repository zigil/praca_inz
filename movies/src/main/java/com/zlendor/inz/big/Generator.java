package com.zlendor.inz.big;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;

public class Generator {
    private final long partSize;
    private final int partCount;
    private final long size;
    private final boolean asc;
    private final String name;
    private final FileSystem fs;
    private final Path outPath;

    public Generator(long partSize, int partCount, String name, String out) throws IOException {
        this.partSize = partSize;
        this.partCount = partCount;
        this.name = name;
        this.asc = name.equals("lens");
        this.size = partSize * partCount;

        fs = FileSystem.get(new Configuration());
        this.outPath = new Path(out);
        if (fs.exists(outPath)) {
            printInfoAndExit("Path exists");
        }
    }

    public static void main(String... args) throws IOException {
        if (args.length != 4) {
            printInfoAndExit();
        }

        final long partSize = Long.valueOf(args[0]);
        final int partCount = Integer.valueOf(args[1]);
        final String name = args[2];
        final String outPath = args[3];

        new Generator(partSize, partCount, name, outPath).generate();
    }

    private void generate() throws IOException {
        fs.mkdirs(outPath);
        long idx = asc ? 0 : size+1;
        for (int i=0; i<partCount; i++) {
            final FSDataOutputStream file = createFile(i);
            for (long j=0; j<partSize; j++) {
                if (asc) idx++; else idx--;
                writeRow(idx, file);
            }
            file.hflush();
            file.close();
        }
    }

    private void writeRow(final long idx, final FSDataOutputStream file) throws IOException {
        final String row =
                "film "+idx + "\t" + name + "\t4.5\t435\r\n";
        file.writeUTF(row);
    }

    private FSDataOutputStream createFile(int idx) throws IOException {
        final String file = "part-"+idx;
        return fs.create(new Path(outPath, file));
    }

    private static void printInfoAndExit() {
        System.out.println("run with 4 args: partSize partCount name[lens/imdb] path");
        throw new RuntimeException("Invalid args");
    }

    private void printInfoAndExit(final String s) throws IOException {
        fs.close();
        throw new RuntimeException(s);
    }

}
