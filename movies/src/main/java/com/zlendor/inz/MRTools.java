package com.zlendor.inz;

import org.apache.commons.io.FileUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import java.io.File;
import java.io.IOException;

public class MRTools {

    public static Job createJob(Configuration conf, String jobName) throws IOException {
        Job job = new Job(conf, jobName);
        job.setJarByClass(MRTools.class);
        return job;
    }

    public static void prepareJobInOut(Job job, String inPath, String outPath) throws java.io.IOException {
        FileInputFormat.addInputPath(job, new Path(inPath));
        FileOutputFormat.setOutputPath(job, new Path(outPath));
        FileUtils.deleteDirectory(new File(outPath));
    }

    public static void prepareJobMultipleInOut(Job job, String inPath, String outPath) throws java.io.IOException {
        FileInputFormat.addInputPaths(job, inPath);
        FileOutputFormat.setOutputPath(job, new Path(outPath));
        FileUtils.deleteDirectory(new File(outPath));
    }

    public static void prepareJobOut(Job job, String outPath) throws java.io.IOException {
        FileOutputFormat.setOutputPath(job, new Path(outPath));
        FileUtils.deleteDirectory(new File(outPath));
    }

    public static void processOrFail(Job job) throws IOException, InterruptedException, ClassNotFoundException {
        if (! job.waitForCompletion(true)) {
            throw new RuntimeException("Job failed");
        }
    }

}
