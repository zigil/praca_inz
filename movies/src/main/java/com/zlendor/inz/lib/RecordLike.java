package com.zlendor.inz.lib;

public interface RecordLike {
    <T extends Comparable<T>> T getField(final int i);
}
