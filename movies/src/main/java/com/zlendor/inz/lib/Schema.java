package com.zlendor.inz.lib;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;

import java.io.Serializable;
import java.util.*;

public class Schema implements Serializable {
    private static final long serialVersionUID = -3102824597304231385L;

    private final Set<Column> columns;
    private final Map<String, Column> columnNames;

    public Schema(Set<Column> columns) {
        this.columns = Collections.unmodifiableSet(new HashSet(columns));

        Map<String, Column> names = new HashMap<>();
        for (Column c: columns) {
            checkUniqueColumnNames(names, c);
            names.put(c.getName(), c);
        }
        this.columnNames = Collections.unmodifiableMap(names);
    }

    public Schema(Column... columns) {
        this(Sets.newHashSet(columns));
    }

    private void checkUniqueColumnNames(Map<String, Column> names, Column c) {
        Preconditions.checkArgument(!names.containsKey(c.getName()), c);
    }

    public Column<?> getColumn(String c) {
        Column column = columnNames.get(c);
        Preconditions.checkNotNull(column);
        return column;
    }

    public boolean hasColumn(String c) {
        return columnNames.containsKey(c);
    }

    public Set<String> getColumnNames() {
        return columnNames.keySet();
    }

    public String[] getColumnNamesAsArray() {
        return columnNames.keySet().toArray(new String[columnNames.size()]);
    }

    @Override
    public String toString() {
        // sort for readability
        StringBuilder sb = new StringBuilder("Schema[");
        List<Column> cols = new ArrayList<>(getColumns());
        Collections.sort(cols);

        for (Column c: cols) {
            sb.append(c.getName()).append(":").append(c.getType().getSimpleName()).append(",");
        }
        sb.setCharAt(sb.length()-1, ']');
        return sb.toString();
    }

    public Schema intersect(String... cols) {
        List<String> colsList = Arrays.asList(cols);
        return intersect(new HashSet<>(colsList));
    }

    public Schema intersect(Set<String> cols) {
        Set<Column> newSet = new HashSet<>();
        for (Column c: this.columns) {
            if (cols.contains(c.getName())) {
                newSet.add(c);
            }
        }
        return new Schema(newSet);
    }

    public Schema intersect(Schema def) {
        return intersect(def.getColumnNames());
    }

    public Schema minus(String... cols) {
        List<String> colsList = Arrays.asList(cols);
        return intersect(new HashSet<>(colsList));
    }

    public Schema minus(Set<String> cols) {
        Set<Column> newSet = new HashSet<>();
        for (Column c: this.columns) {
            if (! cols.contains(c.getName())) {
                newSet.add(c);
            }
        }
        return new Schema(newSet);
    }

    public Schema minus(Schema def) {
        return minus(def.getColumnNames());
    }

    public Set<Column> getColumns() {
        return columns;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Schema)) return false;
        final Schema other = (Schema) o;
        final Object this$columns = this.columns;
        final Object other$columns = other.columns;
        if (this$columns == null ? other$columns != null : !this$columns.equals(other$columns)) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $columns = this.columns;
        result = result * PRIME + ($columns == null ? 0 : $columns.hashCode());
        return result;
    }

}
