package com.zlendor.inz.movies.mapreduce;

import com.zlendor.inz.movies.LensDataParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class PreProcessLensMoviesMapper extends Mapper<LongWritable, Text, Text, Text> {

    private final LensDataParser lensDataParser = new LensDataParser();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String valueAsStr = value.toString();
        if (valueAsStr.startsWith("#")) {
            // skip comments
            return;
        }

        String[] row = lensDataParser.parseImdbMovieRow(valueAsStr);
        if (row == null) {
            // skip invalid format
            return;
        }

        context.write(new Text(row[0].trim()), new Text(row[1]));
    }
}
