package com.zlendor.inz.movies.mapreduce;

import com.zlendor.inz.movies.ComparisonResult;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class CompareImdbAndLensReducer extends Reducer<Text, Text, Text, Text> {

	@Override
	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {

		final String movieTitle = key.toString();

		final CollectedRows collectedRows = new CollectedRows(values, movieTitle).invoke();
		final String[] imdbRow = collectedRows.getImdbRow();
		final String[] lensRow = collectedRows.getLensRow();

		final ComparisonResult comparisonResult = new ComparisonResult(imdbRow, lensRow).invoke();

		context.write(new Text(movieTitle), new Text(comparisonResult.toString()));
	}

	private class CollectedRows {
		private Iterable<Text> values;
		private String movieTitle;
		private String[] imdbRow;
		private String[] lensRow;

		public CollectedRows(Iterable<Text> values, String movieTitle) {
			this.values = values;
			this.movieTitle = movieTitle;
		}

		public String[] getImdbRow() {
			return imdbRow;
		}

		public String[] getLensRow() {
			return lensRow;
		}

		public CollectedRows invoke() {
			imdbRow = null;
			lensRow = null;

			for (Text value: values) {
				final String valueAsStr = value.toString();
				final String[] row = valueAsStr.split("\t");
				if (row[0].equals("imdb")) {
					if (imdbRow != null) {
						throw new RuntimeException("Duplicated imdb row for key " + movieTitle);
					}
					imdbRow = row;
				}
				else if (row[0].equals("lens")) {
					if (lensRow != null) {
						throw new RuntimeException("Duplicated lens row for key " + movieTitle);
					}
					lensRow = row;
				}
				else {
					throw new RuntimeException("Unknown input " + row[0]);
				}
			}
			return this;
		}
	}

}
