package com.zlendor.inz.movies.mapreduce;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

import static com.zlendor.inz.MRTools.*;

public class MoviesFirstMRDriver extends Configured implements Tool {

	@Override
	public int run(String[] args) throws Exception {
		preProcesssImdb();
		aggregateLensRatings();
		preProcesssLens();
		preProcesssUniqueLens();

		compareLensAndImdb();
		summarizeComparison();

		return 0;
	}

	private void aggregateLensRatings() throws IOException, ClassNotFoundException, InterruptedException {
		Job job = createJob(getConf(), "Aggregate Lens Group ratings");
		prepareJobInOut(job, "src/main/resources/lens/ratings", "mr_compared/aggregated_lens_ratings");

		job.setMapperClass(AggregateLensRatingsMapper.class);
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(DoubleWritable.class);

		job.setReducerClass(AggregateLensRatingsReducer.class);
		job.setOutputKeyClass(LongWritable.class);
		job.setOutputValueClass(Text.class);

		processOrFail(job);
	}

	private void preProcesssLens() throws IOException, ClassNotFoundException, InterruptedException {
		Job job = createJob(getConf(), "Preprocess Lens Group movies");
		prepareJobOut(job, "mr_compared/pre_lens");

		MultipleInputs.addInputPath(job, new Path("mr_compared/aggregated_lens_ratings"),
				KeyValueTextInputFormat.class, Mapper.class);

		MultipleInputs.addInputPath(job, new Path("src/main/resources/lens/movies"),
				TextInputFormat.class, PreProcessLensMoviesMapper.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		job.setReducerClass(JoinMoviesRatingsLensReducer.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		processOrFail(job);
	}

	private void preProcesssUniqueLens() throws IOException, ClassNotFoundException, InterruptedException {
		Job job = createJob(getConf(), "Group by title and eliminate duplicates for Lens Group");
		prepareJobInOut(job, "mr_compared/pre_lens", "mr_compared/pre_unique_lens");

		job.setMapperClass(Mapper.class);
		job.setInputFormatClass(KeyValueTextInputFormat.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		job.setReducerClass(PreProcessUniqueLensReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		processOrFail(job);
	}

	private void preProcesssImdb() throws IOException, InterruptedException, ClassNotFoundException {
		Job job = createJob(getConf(), "Preprocess IMDB");
		prepareJobInOut(job, "src/main/resources/imdb", "mr_compared/pre_imdb");

		job.setMapperClass(PreProcessImdbMapper.class);
		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		job.setReducerClass(PreProcessImdbReducer.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		processOrFail(job);
	}

	private void compareLensAndImdb() throws IOException, ClassNotFoundException, InterruptedException {
		Job job = createJob(getConf(), "Compare IMDB and Lens Group");
		prepareJobOut(job, "mr_compared/compared");

		MultipleInputs.addInputPath(job, new Path("mr_compared/pre_unique_lens"),
				KeyValueTextInputFormat.class, Mapper.class);

		MultipleInputs.addInputPath(job, new Path("mr_compared/pre_imdb"),
				KeyValueTextInputFormat.class, Mapper.class);

		job.setMapOutputKeyClass(Text.class);
		job.setMapOutputValueClass(Text.class);

		job.setReducerClass(CompareImdbAndLensReducer.class);
		job.setOutputFormatClass(TextOutputFormat.class);
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(Text.class);

		processOrFail(job);
	}

	private void summarizeComparison() throws IOException, ClassNotFoundException, InterruptedException {
		Job job = createJob(getConf(), "Summarize comparison");
		prepareJobInOut(job, "mr_compared/compared", "mr_compared/summary");

		job.setMapperClass(Mapper.class);
		job.setMapOutputKeyClass(LongWritable.class);
		job.setMapOutputValueClass(Text.class);

		job.setNumReduceTasks(1);
		job.setReducerClass(SummarizeComparisonReducer.class);
		job.setOutputKeyClass(NullWritable.class);
		job.setOutputValueClass(Text.class);

		processOrFail(job);
	}

	public static void main(String[] args) throws Exception {
		int exitCode = ToolRunner.run(new MoviesFirstMRDriver(), args);
		System.exit(exitCode);
	}

}
