package com.zlendor.inz.lib;

import java.io.Serializable;

public class Record implements RecordLike, Serializable {
    private static final long serialVersionUID = -4077173057168944825L;

    private final Comparable<?>[] fields;

    public Record(Comparable<?>... fields) {
        this.fields = fields;
    }

    public <T extends Comparable<T>> T getField(final int i) {
        return (T) fields[i];
    }
}
