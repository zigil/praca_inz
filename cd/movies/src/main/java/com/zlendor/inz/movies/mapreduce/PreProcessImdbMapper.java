package com.zlendor.inz.movies.mapreduce;

import com.zlendor.inz.movies.ImdbDataParser;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class PreProcessImdbMapper extends Mapper<LongWritable, Text, Text, Text> {

    private final ImdbDataParser imdbDataParser = new ImdbDataParser();

    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        final String valueAsStr = value.toString();
        if (valueAsStr.startsWith("#")) {
            // skip comments
            return;
        }

        final String[] row = imdbDataParser.parseLensRow(valueAsStr);

        if (row == null) {
            // skip invalid record
            return;
        }
        if (Boolean.valueOf(row[3]) == Boolean.TRUE) {
            // skip serials
            return;
        }

        String title = row[0].trim();
        if (title.endsWith(" (TV)")) {
            title = title.substring(0, title.length() - " (TV)".length()).trim();
        }
        if (title.endsWith(" (V)")) {
            title = title.substring(0, title.length() - " (V)".length()).trim();
        }

        final StringBuilder sb = new StringBuilder();
        sb.append(row[1])
                .append("\t").append(row[2])
                .append("\t").append(row[3]);

        context.write(new Text(title), new Text(sb.toString()));
    }
}
