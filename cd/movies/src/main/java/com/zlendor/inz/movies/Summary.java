package com.zlendor.inz.movies;

import com.zlendor.inz.movies.ComparisonResult.Comparison;

import java.io.Serializable;

public class Summary implements Serializable {

	private long countEquals = 0l;
	private long countDiff = 0l;

	private long countOnlyImdb = 0l;
	private long countOnlyLens = 0l;
	private long countBothEmpty = 0l;

	private long imdbCount = 0l;
	private long lensCount = 0l;

	public long getCountEquals() {
		return countEquals;
	}

	public long getCountDiff() {
		return countDiff;
	}

	public long getCountOnlyImdb() {
		return countOnlyImdb;
	}

	public long getCountOnlyLens() {
		return countOnlyLens;
	}

	public long getCountBothEmpty() {
		return countBothEmpty;
	}

	public Summary add(Summary s2) {
		this.countEquals += s2.countEquals;
		this.countDiff += s2.countDiff;
		this.countOnlyImdb += s2.countOnlyImdb;
		this.countOnlyLens += s2.countOnlyLens;
		this.countBothEmpty += s2.countBothEmpty;
		this.imdbCount += s2.imdbCount;
		this.lensCount += s2.lensCount;
		return this;
	}

	public Summary add(Comparison comparison) {
		if (comparison == Comparison.BOTH_EMPTY) {
			this.countBothEmpty++;
		}
		else if (comparison == Comparison.EQUALS) {
			imdbCount++;
			lensCount++;
			this.countEquals++;
		}
		else if (comparison == Comparison.NOT_EQUALS) {
			imdbCount++;
			lensCount++;
			this.countDiff ++;
		}
		else if (comparison == Comparison.ONLY_IMDB) {
			imdbCount++;
			this.countOnlyImdb++;
		}
		else if (comparison == Comparison.ONLY_LENS) {
			lensCount++;
			this.countOnlyLens++;
		}
		return this;
	}

	public String toString() {
		return String.format("Summary\r\n" +
						"Number of equal records: %d,\r\n" +
						"Number of diff: %d,\r\n" +
						"Number of records in IMDB: %d,\r\n" +
						"Number of records in Lens Group: %d,\r\n" +
						"Number of records only in IMDB: %d,\r\n" +
						"Number of records only in Lens Group: %d.",
				countEquals, countDiff, imdbCount, lensCount, countOnlyImdb, countOnlyLens);
	}
}
