package com.zlendor.inz.lib;

import com.google.common.base.Preconditions;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class RecordReader implements Serializable {
    private static final long serialVersionUID = -1496047576352098044L;

    private final Schema schema;
    private final Map<String, Integer> fieldNameToIdx;
    private final Map<String, Converter<Comparable<?>,Comparable<?>>> converters;

    /**
     * Column types defined by schema; columns in specified order
     * @param schema
     * @param fields - fields in order
     */
    public RecordReader(Schema schema, String... fields) {
        this.converters = null;
        this.schema = schema;
        fieldNameToIdx = new HashMap<>();
        initFieldsMapping(fields);
    }

    /**
     * Use converters for each column of type String
     * @param converters
     */
    public RecordReader(ColumnConverter<Comparable<?>, Comparable<?>>... converters) {
        Column<?>[] columns = new Column<?>[converters.length];
        Map<String, Integer>fieldNameToIdx = new HashMap<>();
        this.converters = new HashMap<>();

        int i=0;
        for (ColumnConverter<Comparable<?>,Comparable<?>> conv: converters) {
            Class<?> targetClass = conv.getConverter().getTargetClass();
            columns[i] = new Column<>(conv.getName(), targetClass);
            fieldNameToIdx.put(conv.getName(), i);
            this.converters.put(conv.getName(), conv.getConverter());
            i++;
        }
        this.schema = new Schema(columns);
        this.fieldNameToIdx = fieldNameToIdx;
    }

    public RecordReader(Schema schema, Map<String, Integer> fieldNameToIdx) {
        this.converters = null;
        this.schema = schema;
        this.fieldNameToIdx = fieldNameToIdx;
    }

    private void initFieldsMapping(String[] fields) {
        final Map<String, Integer> fieldsMap = new HashMap<>();
        for (int i=0; i<fields.length; i++) {
            fieldsMap.put(fields[i], i);
        }
    }

    private void initFieldsMapping(Column[] columns) {
        final Map<String, Integer> fieldsMap = new HashMap<>();
        for (int i=0; i<columns.length; i++) {
            fieldsMap.put(columns[i].getName(), i);
        }
    }

    public Schema getSchema() {
        return schema;
    }

    public Map<String, Integer> getFieldNameToIdx() {
        return fieldNameToIdx;
    }

    public <T extends Comparable<T>> T getField(final RecordLike record, final String name) {
        Integer idx = fieldNameToIdx.get(name);
        Comparable<?> value = record.getField(idx);

        if (converters != null) {
            Converter<Comparable<?>, Comparable<?>> conv = converters.get(name);
            value = conv.apply(value);
        }

        Preconditions.checkState(value.getClass() == schema.getColumn(name).getType(),
                value.getClass() + " vs " + schema.getColumn(name).getType());
        return (T) value;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (String f: fieldNameToIdx.keySet()) {
            sb.append(f).append(",");
        }
        return  String.format("RecordReader[%s]", sb.substring(0,sb.length()-1));
    }

    public RecordReader intersect(String... cols) {
        return new RecordReader(schema.intersect(cols), fieldNameToIdx);
    }

    public RecordReader minus(String... cols) {
        return new RecordReader(schema.minus(cols), fieldNameToIdx);
    }

    public static class ColumnConverter<F, T> {
        private final String name;
        private final Converter<F, T> converter;

        public ColumnConverter(String name, Converter<F, T> converter) {
            this.name = name;
            this.converter = converter;
            Preconditions.checkNotNull(name);
            Preconditions.checkNotNull(converter);
        }

        /**
         * Use default converter from String
         */
        public ColumnConverter(String name, Class<?> to) {
            this(name, Converter.FROM_STRING_CONVERTERS.get(to));
        }

        public String getName() {
            return name;
        }

        public Converter<F, T> getConverter() {
            return converter;
        }
    }
}
