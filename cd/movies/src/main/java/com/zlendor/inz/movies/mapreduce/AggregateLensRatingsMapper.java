package com.zlendor.inz.movies.mapreduce;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class AggregateLensRatingsMapper extends Mapper<LongWritable, Text, LongWritable, DoubleWritable> {
    @Override
    public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String valueAsStr = value.toString();
        if (valueAsStr.startsWith("#")) {
            // skip comments
            return;
        }

        String[] row = valueAsStr.split(",");
        Long movieId = Long.valueOf(row[1]);

        // normalization: rating multiplied by 2 to much Lens Group scale
        Double rating = Double.valueOf(row[2]) * 2;
        context.write(new LongWritable(movieId), new DoubleWritable(rating));
    }
}