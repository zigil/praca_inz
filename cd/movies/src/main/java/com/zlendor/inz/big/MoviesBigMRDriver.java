package com.zlendor.inz.big;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.input.MultipleInputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

import static com.zlendor.inz.MRTools.*;
import static com.zlendor.inz.MRTools.processOrFail;

public class MoviesBigMRDriver extends Configured implements Tool {

    @Override
    public int run(String[] args) throws Exception {
        if (args.length < 4) {
            System.out.println("require 4 args: left right out reducers");
            return -1;
        }

        final int reducers = Integer.valueOf(args[3]);
        final String comparisonOut = args[2]+"/comparison";
        final String summaryOut = args[2]+"/summary";

        compare(args[0], args[1], comparisonOut, reducers);
        summarizeComparison(comparisonOut, summaryOut, reducers);

        return 0;
    }

    private void compare(final String left, final String right, final String out, int reducers) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = createJob(getConf(), "Compare MR - test");
        prepareJobOut(job, out);

        MultipleInputs.addInputPath(job, new Path(left),
                KeyValueTextInputFormat.class, Mapper.class);

        MultipleInputs.addInputPath(job, new Path(right),
                KeyValueTextInputFormat.class, Mapper.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);

        job.setReducerClass(CompareReducer.class);
        job.setOutputFormatClass(TextOutputFormat.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(reducers);

        processOrFail(job);
    }

    private void summarizeComparison(final String in, final String out, int reducers) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = createJob(getConf(), "Comparison summary - test");
        prepareJobInOut(job, in, out);

        job.setMapperClass(Mapper.class);
        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(Text.class);

        job.setNumReduceTasks(1);
        job.setReducerClass(SummarizeReducer.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        job.setNumReduceTasks(reducers);

        processOrFail(job);
    }

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new MoviesBigMRDriver(), args);
        System.exit(exitCode);
    }

}
