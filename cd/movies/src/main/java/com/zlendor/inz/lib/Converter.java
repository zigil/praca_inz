package com.zlendor.inz.lib;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public abstract class Converter<F, T> implements Function<F, T>, Serializable {
    private static final long serialVersionUID = 2826864945127418248L;

    private Class<T> clazz;

    public Converter(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Class<T> getTargetClass() {
        return clazz;
    }

    public static final Map<Class, Converter> FROM_STRING_CONVERTERS;
    static {
        FROM_STRING_CONVERTERS = new HashMap<>();

        FROM_STRING_CONVERTERS.put(LocalDate.class, new Converter<String, LocalDate>(LocalDate.class) {
            @Override
            public LocalDate apply(String input) {
                return LocalDate.parse(input);
            }
        });
        FROM_STRING_CONVERTERS.put(Integer.class, new Converter<String, Integer>(Integer.class) {
            @Override
            public Integer apply(String input) {
                return Integer.valueOf(input);
            }
        });
        FROM_STRING_CONVERTERS.put(Long.class, new Converter<String, Long>(Long.class) {
            @Override
            public Long apply(String input) {
                return Long.valueOf(input);
            }
        });
        FROM_STRING_CONVERTERS.put(BigDecimal.class, new Converter<String, BigDecimal>(BigDecimal.class) {
            @Override
            public BigDecimal apply(String input) {
                return new BigDecimal(input);
            }
        });
        FROM_STRING_CONVERTERS.put(Double.class, new Converter<String, Double>(Double.class) {
            @Override
            public Double apply(String input) {
                return Double.valueOf(input);
            }
        });
        FROM_STRING_CONVERTERS.put(String.class, new Converter<String, String>(String.class) {
            @Override
            public String apply(String input) {
                return input;
            }
        });
        FROM_STRING_CONVERTERS.put(Timestamp.class, new Converter<String, Timestamp>(Timestamp.class) {
            @Override
            public Timestamp apply(String input) {
                return Timestamp.valueOf(LocalDateTime.parse(input.substring(0, 10)));
            }
        });
    }

}
