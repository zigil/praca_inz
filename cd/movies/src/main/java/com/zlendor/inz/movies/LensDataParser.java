package com.zlendor.inz.movies;

import java.io.Serializable;

public class LensDataParser implements Serializable {
	public String[] parseImdbMovieRow(String row) {
		final int firstComma = row.indexOf(",");
		if (row.charAt(firstComma+1) == '\"') {
			final int firstQuotation = firstComma+1;
			final int lastQuotation = row.indexOf("\"", firstQuotation+1);
			return new String[] {
					row.substring(0,firstComma),
					row.substring(firstQuotation+1, lastQuotation),
					row.substring(lastQuotation+2, row.length())
			};
		}
		else {
			return row.split(",");
		}
	}
}
