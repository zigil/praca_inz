package com.zlendor.inz.lib;

import java.io.Serializable;

public class Column<T> implements Comparable<Column>, Serializable {
    private static final long serialVersionUID = 1L;

    private final String name;
    private final Class<T> type;

    public Column(String name, Class<T> type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    // for sorting
    @Override
    public int compareTo(Column o) {
        return this.name.compareTo(o.getName());
    }

    public Class<T> getType() {
        return type;
    }

    public boolean equals(Object o) {
        if (o == this) return true;
        if (!(o instanceof Column)) return false;
        final Column other = (Column) o;
        if (!other.canEqual((Object) this)) return false;
        final Object this$name = this.getName();
        final Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) return false;
        final Object this$type = this.type;
        final Object other$type = other.type;
        if (this$type == null ? other$type != null : !this$type.equals(other$type)) return false;
        return true;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final Object $name = this.getName();
        result = result * PRIME + ($name == null ? 0 : $name.hashCode());
        final Object $type = this.type;
        result = result * PRIME + ($type == null ? 0 : $type.hashCode());
        return result;
    }

    protected boolean canEqual(Object other) {
        return other instanceof Column;
    }
}
