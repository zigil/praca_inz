package com.zlendor.inz.movies;

import java.math.BigDecimal;

public class ComparisonResult {
	public static final BigDecimal MAX_DIFF = new BigDecimal("0.5");

	public enum Comparison {
		EQUALS, NOT_EQUALS, ONLY_IMDB, ONLY_LENS, BOTH_EMPTY}

	private Double[] imdbRow;
	private Double[] lensRow;

	private BigDecimal imdbRank;
	private BigDecimal lensRank;

	private Comparison comparison;
	private BigDecimal sumRank;
	private BigDecimal sumVotes;

	// from Haddoop
	public ComparisonResult(String[] imdbRow, String[] lensRow) {
		if (imdbRow != null && ! imdbRow[1].equals("null") && ! imdbRow[1].equals("NaN")
				&& ! imdbRow[2].equals("null") && ! imdbRow[2].equals("NaN")) {
			this.imdbRow = new Double[]{
					Double.valueOf(imdbRow[1]),
					Double.valueOf(imdbRow[2])
			};
		}
		else {
			this.imdbRow = null;
		}
		if (lensRow != null && ! lensRow[1].equals("null") && ! lensRow[1].equals("NaN")
				&& ! lensRow[2].equals("null") && ! lensRow[2].equals("NaN")) {
			this.lensRow = new Double[]{
					Double.valueOf(lensRow[1]),
					Double.valueOf(lensRow[2])
			};
		}
		else {
			this.lensRow = null;
		}
	}

	// fom Spark
	public ComparisonResult(Double[] imdbRow, Double[] lensRow) {
		this.imdbRow = imdbRow;
		this.lensRow = lensRow;
	}

	public BigDecimal getImdbRank() {
		return imdbRank;
	}

	public BigDecimal getLensRank() {
		return lensRank;
	}

	public Comparison getComparison() {
		return comparison;
	}

	public BigDecimal getSumRank() {
		return sumRank;
	}

	public BigDecimal getSumVotes() {
		return sumVotes;
	}

	public ComparisonResult invoke() {
		try {
			return _invoke();
		}
		catch (NumberFormatException e) {
			String imdbInfo = imdbRow == null ? "null" : imdbRow[0] + "/" + imdbRow[1];
			String lensInfo = lensRow == null ? "null" : lensRow[0] + "/" + lensRow[1];
			System.err.println(imdbInfo + "\t\t" + lensInfo);
			throw e;
		}
	}

	public ComparisonResult _invoke() {
		final boolean isImdb = imdbRow != null;
		final boolean isLens = lensRow != null;

		imdbRank = isImdb ?
				new BigDecimal(imdbRow[0]).setScale(2, BigDecimal.ROUND_HALF_EVEN)
				: null;
		lensRank = isLens ?
				new BigDecimal(lensRow[0]).setScale(2, BigDecimal.ROUND_HALF_EVEN)
				: null;

		final BigDecimal imdbVotes = isImdb ? new BigDecimal(imdbRow[1]) : null;
		final BigDecimal lensVotes = isLens ? new BigDecimal(lensRow[1]) : null;

		final boolean bothEmpty = !isImdb && !isLens;
		final boolean bothNonEmpty = isImdb && isLens;

		sumRank = null;
		sumVotes = null;

		if (bothEmpty) {
			comparison = Comparison.BOTH_EMPTY;
		}
		else if (!bothNonEmpty) {
			if (isImdb) {
				sumRank = imdbRank;
				sumVotes = imdbVotes;
				comparison = Comparison.ONLY_IMDB;
			}
			else {
				sumRank = lensRank;
				sumVotes = lensVotes;
				comparison = Comparison.ONLY_LENS;
			}
		}
		else {
			sumVotes = imdbVotes.add(lensVotes);
			sumRank =
					imdbRank.multiply(imdbVotes)
							.add(lensRank.multiply(lensVotes))
							.divide(sumVotes, BigDecimal.ROUND_HALF_EVEN)
							.setScale(2, BigDecimal.ROUND_HALF_EVEN);
			final boolean areEqual = imdbRank.subtract(lensRank).abs().compareTo(MAX_DIFF) <= 0;
			comparison = areEqual ? Comparison.EQUALS : Comparison.NOT_EQUALS;
		}
		return this;
	}

	public String toString() {
		return this.getComparison().name() +
				"\tlens: " + this.getLensRank() +
				"\timdb: " + this.getImdbRank() +
				"\tavg: "  + this.getSumRank() +
				"\tvotes:" + this.getSumVotes();
	}
}
