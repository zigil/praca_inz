package com.zlendor.inz.big;

import com.zlendor.inz.movies.ComparisonResult.Comparison;
import com.zlendor.inz.movies.Summary;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class SummarizeReducer extends Reducer<LongWritable, Text, NullWritable, Text> {

	private final Summary summary = new Summary();

	/**
	 * Summarize results of comparison
	 */
	@Override
	public void reduce(LongWritable key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {

		for (Text value: values) {
			String[] row = value.toString().split("\t");
			Comparison result = Comparison.valueOf(row[1]);
			summary.add(result);
		}
	}

	@Override
	protected void cleanup(Context context) throws IOException, InterruptedException {
		final String outValue = summary.toString();
		context.write(NullWritable.get(), new Text(outValue));
	}
}
