package com.zlendor.inz.movies;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ImdbDataParser implements Serializable {
    private static final Pattern LANS_FRONT = Pattern.compile("(\\d+) +(\\d\\.\\d) +(.+)");

    public String[] parseLensRow(String row) {
        final String skippedDistribution = row.substring(10).trim();
        final Matcher matcher = LANS_FRONT.matcher(skippedDistribution);
        if (! matcher.find()) {
            return null;
        }
        else {
            final String votes = matcher.group(1);
            final String rank = matcher.group(2);
            String rest = matcher.group(3).trim();

            boolean isSerial = false;
            if (rest.charAt(rest.length()-1) == '}') {
                // drop off episode
                rest = rest.substring(0, rest.lastIndexOf('{')).trim();
                isSerial = true;
            }
            final String movie = rest.replaceAll("\"", "");
            return new String[] {movie, rank, votes, String.valueOf(isSerial)};
        }
    }

}
