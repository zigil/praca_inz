package com.zlendor.inz.movies.mapreduce;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class PreProcessUniqueLensReducer extends Reducer<Text, Text, Text, Text> {
	/**
	 * There are problems in IMDB data set as some movies are multiplicated.
	 * Let's count an average for them
	 */
	@Override
	public void reduce(Text key, Iterable<Text> values, Context context)
			throws IOException, InterruptedException {

		long votes = 0l;
		double sum = 0d;
		for (Text value: values) {
			String[] row = value.toString().split("\t");
			if (! row[0].equals("null")) {
				double addRank = Double.parseDouble(row[0]);
				long addVotes = Long.parseLong(row[1]);
				sum += addRank * addVotes;
				votes += addVotes;
			}
		}

		String outValue = "lens\t" + sum/votes + "\t" + votes;
		context.write(key, new Text(outValue));
	}
}
