package com.zlendor.inz.movies.mapreduce;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class JoinMoviesRatingsLensReducer extends Reducer<Text, Text, Text, Text> {
    @Override
    public void reduce(Text key, Iterable<Text> values, Context context)
            throws IOException, InterruptedException {

        String movieTitle = null;
        Double avg = null;
        Long votes = null;

        int count = 0;
        for (Text value: values) {
            count++;

            String valueAsStr = value.toString();
            String[] row = valueAsStr.split("\t");

            // check which collection is it
            if (row.length == 1) {
                movieTitle = row[0];
            }
            else if (row.length == 2) {
                avg = Double.valueOf(row[0]);
                votes = Long.valueOf(row[1]);
            }
            else {
                throw new RuntimeException("Non expected input " + row.length);
            }
        }

        if (count!=1 && count!=2) {
            throw new RuntimeException("Expected 2 records: " + count);
        }

        String out = String.format("%s\t%d", avg, votes);
        context.write(new Text(movieTitle), new Text(out));
    }
}
