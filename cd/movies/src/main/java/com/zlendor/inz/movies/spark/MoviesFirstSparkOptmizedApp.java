package com.zlendor.inz.movies.spark;

import com.google.common.base.Optional;
import com.zlendor.inz.movies.ComparisonResult;
import com.zlendor.inz.movies.LensDataParser;
import com.zlendor.inz.movies.ImdbDataParser;
import com.zlendor.inz.movies.Summary;
import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

public class MoviesFirstSparkOptmizedApp {
	public static void main(final String[] args) throws IOException {
		final SparkConf conf = new SparkConf()
				.setAppName("Test")
				.setMaster("local")
				.set("spark.local.ip", "127.0.0.1")
				.set("spark.driver.host", "127.0.0.1");

		final JavaSparkContext sc = new JavaSparkContext(conf);
		FileUtils.deleteDirectory(new File("rdd_opt_compared"));

		// [title => [rank, votes]]
		final JavaPairRDD<String, Double[]> imdbRecordRdd = prepareImdb(sc);

		// [movieId => [rank, votes]]
		final JavaPairRDD<Long, Double[]> lensMovieRatingsRdd = prepareLensAggregatedRatings(sc);

		// [movieId, title, genres]
		final JavaPairRDD<Long, String> lensMovieRecordRdd = prepareLensMovies(sc);

		final JavaPairRDD<Long, Tuple2<Double[], String>> lensJoined = lensMovieRatingsRdd
				.join(lensMovieRecordRdd);

		// [title => [rank, votes]]
		final JavaPairRDD<String, Double[]> lensRecordRdd = lensJoined
				.mapToPair(joined -> new Tuple2<>(joined._2()._2(), joined._2()._1()))
				.cache();

		final JavaPairRDD<String, Tuple2<Optional<Double[]>, Optional<Double[]>>> joinedRdd = imdbRecordRdd
				.<Double[]>fullOuterJoin(lensRecordRdd);

		final JavaPairRDD<String, ComparisonResult> comparisonRdd = joinedRdd
				.mapValues(records -> compare(records))
				.cache();

		comparisonRdd
				.mapValues(result -> result.toString())
				.saveAsTextFile("rdd_opt_compared");

		Summary summary = comparisonRdd.aggregate(
				new Summary(),
				(s, r) -> s.add(r._2().getComparison()),
				(s1, s2) -> s1.add(s2));

		System.out.println(summary.toString());
	}

	private static ComparisonResult compare(final Tuple2<Optional<Double[]>, Optional<Double[]>> records) {
		return new ComparisonResult(records._1().orNull(), records._2().orNull()).invoke();
	}

	// [movieId => [rank, votes]]
	private static JavaPairRDD<Long, Double[]> prepareLensAggregatedRatings(final JavaSparkContext sc) {
		// [userId, movieId, rating, timestamp]
		final JavaRDD<String[]> lensRatingsRdd = sc.textFile("src/main/resources/lens/ratings")
				.filter(row -> !row.startsWith("#"))
				.map(row -> row.trim().split(","));

		final JavaPairRDD<Long, Double> rateRdd = lensRatingsRdd
				.mapToPair(imdRating -> new Tuple2<>(Long.valueOf(imdRating[1]), Double.valueOf(imdRating[2])));

		final JavaPairRDD<Long, Votes> preparedRdd = rateRdd.combineByKey(
				v -> new Votes(v),
				(acc, v) -> acc.add(v),
				(acc1, acc2) -> acc1.add(acc2)
		);
		return preparedRdd.mapValues(v -> new Double[]{
				v.getVotes() == 0 ? null : v.getSum() / v.getVotes(),
				new Double(v.getVotes())});
	}

	private static JavaPairRDD<Long, String> prepareLensMovies(final JavaSparkContext sc) {
		final LensDataParser lensDataParser = new LensDataParser();

		final JavaRDD<String[]> lensMoviesRdd = sc.textFile("src/main/resources/lens/movies")
				.filter(row -> ! row.startsWith("#"))
				.map(row -> lensDataParser.parseImdbMovieRow(row.trim()));

		return lensMoviesRdd
				.mapToPair(record -> new Tuple2<>(Long.valueOf(record[0]), record[1]));
	}

	private static JavaPairRDD<String, Double[]> prepareImdb(final JavaSparkContext sc) {
		final ImdbDataParser imdbDataParser = new ImdbDataParser();

		// title, rank, votes, isSerial
		final JavaRDD<String[]> imdbRowsRdd = sc.textFile("src/main/resources/imdb")
				.filter(row -> !row.startsWith("#"))
				.map(row -> imdbDataParser.parseLensRow(row.trim()))
				.filter(row -> row != null
						&& ! Boolean.valueOf(row[3]));

		return imdbRowsRdd
				.mapToPair(record -> new Tuple2<>(record[0], new Double[]{
						Double.valueOf(record[1]),
						Double.valueOf(record[2])}))
				.cache();
	}

	private static class Votes implements Serializable {
		private int votes;
		private double sum;

		public Votes(int votes, double sum) {
			this.votes = votes;
			this.sum = sum;
		}

		public Votes(Double v) {
			sum = v;
			votes = 1;
		}

		public int getVotes() {
			return votes;
		}

		public double getSum() {
			return sum;
		}

		public Votes add(Double v) {
			votes++;
			sum += v;
			return this;
		}

		public Votes add(Votes acc2) {
			this.sum += acc2.sum;
			this.votes += acc2.votes;
			return this;
		}
	}
}
