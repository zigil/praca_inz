package com.zlendor.inz.movies.mapreduce;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class AggregateLensRatingsReducer extends Reducer<LongWritable, DoubleWritable, LongWritable, Text> {
    @Override
    public void reduce(LongWritable key, Iterable<DoubleWritable> values, Context context)
            throws IOException, InterruptedException {

        long count = 0;
        double sum = 0d;
        for (DoubleWritable value: values) {
            sum += value.get();
            count++;
        }

        double avg = sum / count;
        String out = avg + "\t" + count;

        context.write(key, new Text(out));
    }

}
