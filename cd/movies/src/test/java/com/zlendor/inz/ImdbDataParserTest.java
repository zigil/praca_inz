package com.zlendor.inz;

import com.zlendor.inz.movies.ImdbDataParser;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ImdbDataParserTest {
	private final ImdbDataParser parser = new ImdbDataParser();

	@Test
	public void parseLansRow_noQuotations() {
		final String[] parsed = parser.parseLensRow("0000000125  1580922   9.2  The Shawshank Redemption (1994)");
		assertThat(parsed).containsExactly("The Shawshank Redemption (1994)", "9.2", "1580922", "false");
	}

	@Test
	public void parseLansRow_noEpisode_withQuotation() {
		final String[] parsed = parser.parseLensRow("1000000102      62   6.2  \"#1 Single\" (2006)");
		assertThat(parsed).containsExactly("#1 Single (2006)", "6.2", "62", "false");
	}

	@Test
	public void parseLansRow_withEpisode_withQuotation() {
		final String[] parsed = parser.parseLensRow("      0000000223     637   8.6  \"24\" (2001) {Day 4: 12:00 p.m.-1:00 p.m. (#4.6)}");
		assertThat(parsed).containsExactly("24 (2001)", "8.6", "637", "true");
	}

	@Test
	public void parseLansRow_withEpisode_noQuotation() {
		final String[] parsed = parser.parseLensRow("      0000000223     637   8.6  24 (2001) {Day 4: 12:00 p.m.-1:00 p.m. (#4.6)}");
		assertThat(parsed).containsExactly("24 (2001)", "8.6", "637", "true");
	}

	@Test
	public void parseLansRow_withEpisode_withQuotation_tv() {
		final String[] parsed = parser.parseLensRow("      1000001112      68   6.9  Signed, Sealed, Delivered: The Impossible Dream (2015) (TV)");
		assertThat(parsed).containsExactly("Signed, Sealed, Delivered: The Impossible Dream (2015) (TV)", "6.9", "68", "false");
	}

}
