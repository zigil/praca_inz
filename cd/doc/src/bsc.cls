\NeedsTeXFormat{LaTeX2e}

\ProvidesClass{bsc}
\DeclareOption{pdflatex}{\pdflatextrue}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ProcessOptions\LoadClass[a4paper,twoside,openright]{report}

%\RequirePackage[showframe]{geometry}
\RequirePackage{geometry}
\geometry{verbose, a4paper, %
  tmargin=3.0cm, headsep=1.5em, headheight=14pt, %
  bmargin=2.5cm, footskip=2.5em, %
  lmargin=2.0cm, rmargin=2.0cm, %
  bindingoffset=1.0cm, nomarginpar 
}

\RequirePackage{emptypage}

\RequirePackage{fancyhdr}
\pagestyle{fancy}
\fancyhf{}
\fancyhead[LO,RE]{\small\nouppercase{\leftmark}}
%\fancyhead[RO,LE]{\small\nouppercase{\rightmark}}
\fancyfoot[LE,RO]{\thepage}
\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0pt}
\renewcommand{\chaptermark}[1]{\markboth{\thechapter.\space#1}{}} 

\fancypagestyle{plain}{%
  \fancyhf{} % clear all header and footer fields
  \fancyfoot[LE,RO]{\thepage}
  \renewcommand{\headrulewidth}{0pt}
}

\RequirePackage{caption}
\captionsetup{font=small,format=plain,labelsep=period,figurename=Rysunek,tablename=Tabela}

\RequirePackage{listings}
\lstset{
  frame=top,
  basicstyle=\normalfont\footnotesize\sffamily,    % the size of the fonts that are used for the code
  stepnumber=1,                           % the step between two line-numbers. If it is 1 each line will be numbered
  numbersep=10pt,                         % how far the line-numbers are from the code
  tabsize=4,                              % tab size in blank spaces
  breaklines=true,                        % sets automatic line breaking
  captionpos=t,                           % sets the caption-position to top
  mathescape=true,
  stringstyle=\color{white}\ttfamily, 
  showspaces=false,
  showtabs=false,
  xleftmargin=17pt,
  framexleftmargin=17pt,
  framexrightmargin=17pt,
  framexbottommargin=10pt,
  framextopmargin=10pt,
  showstringspaces=false,
  literate={ą}{{\k{a}}}1
	    {Ą}{{\k{A}}}1
	    {ę}{{\k{e}}}1
	    {Ę}{{\k{E}}}1
	    {ó}{{\'o}}1
	    {Ó}{{\'O}}1
	    {ś}{{\'s}}1
	    {Ś}{{\'S}}1
	    {ł}{{\l{}}}1
	    {Ł}{{\L{}}}1
	    {ż}{{\.z}}1
	    {Ż}{{\.Z}}1
	    {ź}{{\'z}}1
	    {Ź}{{\'Z}}1
	    {ć}{{\'c}}1
	    {Ć}{{\'C}}1
	    {ń}{{\'n}}1
	    {Ń}{{\'N}}1
}
\renewcommand{\lstlistingname}{Plik}

\RequirePackage{enumitem}
\setlist{leftmargin=3em, itemsep=0em, topsep=0em}

\RequirePackage{titlesec}
\titleformat{\chapter}{\normalfont\LARGE\bfseries}{\thechapter.}{1em}{}[\vspace{2pt}{\titlerule[.8pt]}]
%\titleformat{\chapter}{\normalfont\LARGE\bfseries{\titlerule[.8pt]}\vspace{4pt}}{\thechapter.}{1em}{}[\vspace{4pt}{\titlerule[.8pt]}]
%\titleformat{\chapter}{\normalfont\LARGE\bfseries}{\thechapter.}{1em}{}
\titleformat{\subsubsection}{\normalfont\bfseries\itshape}{\thechapter.}{1em}{}[]
\titlespacing\chapter{0pt}{-2em}{2.5em}
\titlespacing\section{0pt}{1.5em}{1em}
\titlespacing\subsection{\parindent}{1em}{.5em}
\titlespacing\subsubsection{\parindent}{.5em}{.5pt}

\RequirePackage{setspace}
\onehalfspacing

\newcommand\updatepdfmeta{
  \ifpdf
    \pdfcompresslevel=9
    \hypersetup{pdfauthor={\@author},pdftitle={\@title}}
  \fi
} 
\AtBeginDocument{\updatepdfmeta}

\frenchspacing

\renewcommand{\partname}{}
\renewcommand{\thepart}{}

\RequirePackage{tabularx}
\newcolumntype{L}[1]{>{\hsize=#1\hsize\raggedright\arraybackslash}X}%
\newcolumntype{R}[1]{>{\hsize=#1\hsize\raggedleft\arraybackslash}X}%
\newcolumntype{C}[1]{>{\hsize=#1\hsize\centering\arraybackslash}X}%

% custom commands
\newcommand{\dotline}[2]{\changefont{10}\makebox[#1][#2]{\dotfill}}
\newcommand{\changefont}[1]{\fontsize{#1}{#1}\selectfont}
\newcommand*{\ral}[2]{\overrightarrow{\makebox[#1]{$_{#2}$}}}
\newcommand*{\ra}[1]{\ral{6em}{#1}}
\newcommand*{\textbfit}[1]{\textbf{\textit{#1}}}

