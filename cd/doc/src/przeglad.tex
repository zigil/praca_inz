\chapter{\textit{Hadoop}}
\label{hadoop}

\section{O Hadoop}
W 2004 roku firma \textit{Google} opublikowała pracę \cite{GoogleMR}, w~której opisała model przetwarzania \textit{Map/Reduce}. Koncepcja ta opiera się na algorytmie \textit{Map/Reduce} i zakłada możliwość przetwarzania bardzo dużych danych, dużą skalowalność poziomą poprzez przetwarzanie rozproszone, wysoką odporność na awarie m.in. dzięki stosowaniu replikacji danych (czyli powielenia danych na wielu węzłach w~sieci) oraz systemowi wykrywania awarii węzłów \cite{HadoopGuide}. 

Projekt \textit{Hadoop} powstał jako próba realizacji tej koncepcji. Szybko odniósł duży sukces, dzięki wielu wdrożeniom powstawało z~czasem coraz więcej nowych projektów rozszerzających jego funkcjonalność (nowe sposoby serializacji, sposoby zapisu danych itp.). Proces ten znacznie przyspieszył w~momencie ukazania się w roku 2012 drugiej wersji \cite{HadoopEsse}, w~której wyodręb\-niono komponent \textit{YARN} \ref{yarn} (ang. \textit{Yet Another Resource Negotiator}) do zarządzania zasobami unieza\-leżniając wykonywanie zadań od algorytmu \textit{Map/Reduce} \cite{Hadoop}. Otworzyło to drogę do adaptacji nowych modeli przetwarzania jak \textit{Spark}.

Dlatego dzisiaj mówi się raczej o~platformie (lub ekosystemie) \textit{Hadoop}, w~skład której wchodzą bazowe komponenty oraz w~ramach której można dobrać bardzo szeroki wachlarz technologii. Platforma dostępna jest jako wolne oprogramowanie na licencji \textit{Apache License 2.0}, przy czym pojawiły się komercyjne dystrybucje ze wsparciem jak \textit{Cloudera, Hortonworks, Pivotal} i~inne.

\section{System plików \textit{HDFS}}
\textit{Hadoop} umożliwia działanie na wielu rodzajach systemów plików, m.in. lokalny system (przydatne podczas testowania) oraz rozproszony system plików \textit{HDFS}. Ten ostatni posiada następujące właściwości \cite{HadoopEsse}:
\begin{itemize}
 \item skalowalność -- bardzo łatwo zwiększyć pojemność (bez utraty wydajności w~dostępie do danych) poprzez dodanie kolejnych węzłów;
 \item odporność na awarie -- dane są \textit{replikowane} (zapisywane są w~wielu kopiach na różnych węzłach), wbudowany mechanizm do wykrywania awarii węzła i~obsługa bez konieczności unieruchomienia całego klastra;
 \item dostępność -- system nie wymaga specjalistycznego sprzętu ani oprogramowania, w~skład klastra mogą wchodzić różne węzły, co do których są niskie wymagania;
 \item elastyczność -- duże klastry można podzielić w~celu łatwiejszego zarządzania na federacje; bardzo łatwo dodawać i~usuwać poszczególne węzły;
 \item duży rozmiar danych -- może przechowywać bardzo duże ilości danych; istnieje możliwość wykonywania obliczeń bezpośrednio na węzłach bez konieczności przesyłania danych przez sieć (ang. \textit{data locality access});
 \item efektywność -- system wspiera możliwość wielokrotnego odczytu kosztem możliwości modyfikowania danych (ang. \textit{write once, read many}), wspiera również dużą przepustowość danych kosztem czas dostępu (stworzony jest do masowego przetwarzania dużych danych, nie do budowania systemów czasu rzeczywistego).
\end{itemize}

Dane trzymane są w blokach o dużym rozmiarze (domyślnie 64 MB), które są najmniejszą jednostką przetwarzania. W~rozproszonym systemie istnieją dwa rodzaje węzłów: \textit{namenode} oraz \textit{datanode}. Pierwsze przechowują informacje o~strukturze systemu plików (drzewo katalogów, jakie pliki są przechowywane i~z~jakich bloków się składają), drugie przechowują konkretne bloki (dane). Istnieje możliwość uruchomienia dodatkowego serwera \textit{namenode} (ang. \textit{secondary namenode}), który w~razie awarii pierwszego przejmie jego rolę. 

Dostęp do systemu plików może odbyć się na wiele sposobów, m.in.: 
\begin{itemize}
 \item poprzez \textit{API} dla programów \textit{Java};
 \item z linii komend;
 \item poprzez interfejs \textit{HTTP REST API}.
\end{itemize}

\subsection{Odczyt pliku}
Proces przebiegu odczytu pliku przedstawiono na rysunku \ref{fig:hdfs_read}
\begin{figure}[h]
\centering
\includegraphics[width=12cm]{hdfs_read}
\caption{Czytanie pliku z \textit{HDFS} \cite{HadoopGuide}}
\label{fig:hdfs_read}
\end{figure}                    

\subsection{Zapis pliku}
Proces przebiegu zapisu pliku przedstawiono na rysunku \ref{fig:hdfs_write}
\begin{figure}[h]
\centering
\includegraphics[width=12cm]{hdfs_write}
\caption{Czytanie pliku z \textit{HDFS} \cite{HadoopGuide}}
\label{fig:hdfs_write}
\end{figure}                    

\section{Sposób zapisu danych}
Wcześnie omówiono system plików, w~tej sekcji omówiony zostanie sposób, w~jaki dane zapisywane są w~plikach. O~ile najmniejszą fizyczną jednostką zapisu pliku jest blok, to najmniejszą logiczną jednostką zapisu jest rekord, który ma postać klucz--wartość. 

Dodatkowo, w~celu umożliwienia przetwarzania równoległego na wielu węzłach, wprowadzono po\-jęcie \textit{split'u}. Jest to fragment pliku, który będzie przetwarzany przez jeden proces. Domyślnie jego rozmiar zbliżony jest (z~przybliżeniem do rozmiaru rekordu, który może ,,wystawać'' poza pojedynczy \textit{split}) do rozmiaru bloku, dzięki czemu jeden proces czyta pojedynczy blok. Jednak istnieje również możliwość zmiany rozmiaru bloku i~\textit{split'u}.

Istnieje wiele różnych formatów plików, mających swoją reprezentację w~klasach \textit{Java} dziedziczących po \textit{FileInputFormat} oraz \textit{FileOutputFormat}. Ich odpowiedzialnością jest wyznaczanie sposobu \textit{splitowania} oraz dostarczanie klas pozwalających na czytanie i~zapis poszczególnych rekordów.

Do podstawowych formatów należą:
\begin{itemize}
 \item \textit{Text} -- obsługuje plik w~formacie zwykłego pliku tekstowego, gdzie każdy rekord pamię\-tany jest w~nowej linii;
 \item \textit{SequenceFile} -- format stworzony przez \textit{Hadoop}, obsługuje dane tekstowe i~binarne;
 \item \textit{MapFile} -- podobny do \textit{SequenceFile}, ale posortowany po kluczu z dodatkowym plikiem-indeksem pozwalającym na szybkie odszukiwanie kluczy;
 \item \textit{SetFile, ArrayFile, BloomMapFile} -- inne odmiany \textit{MapFile} \cite{HadoopGuide}.
\end{itemize}

Istnieje wiele innych formatów, jak \textit{Avro} pozwalający na prosty odczyt w~językach programowania innych niż \textit{Java}. Istnieją również formaty kolumnowe jak \textit{Parquet}, gdzie dane w~obrębie jednego \textit{splitu} zapisane są po kolei kolumnami, a~nie wierszami. Więcej informacji można znaleźć w~pozycji \cite{HadoopGuide}.

\subsection{Kompresja danych}
Zapisywane dane mogą być dodatkowo kompresowane, co nie tylko zmniejsza ilość miejsca, jakie zajmują na dysku, ale również przyspiesza transfer przez sieć: \textit{gzip, bzip2, LZO, LZ4, Snappy} i~inne \cite{HadoopGuide}.

\subsection{Serializacja obiektów}
Proces zapisu informacji reprezentowanej w pamięci na dysk wymaga wcześniejszej konwersji danych do postaci strumienia bajtów, która nazywana jest serializacją. Wbudowana serializacja w \textit{Java} niesie ograniczenia: jest stosunkowo mało wydajna czasowo i~pojemnościowo (zserializowane dane zajmują dużo miejsca na dysku). Dlatego powstało wiele innych realizacji, m.in.:
\begin{itemize}
 \item Kryo -- bardzo wydajny i zwięzły sposób serializacji dla \textit{Java};
 \item Avro -- pozwalający odczytywać dane również w innych językach niż \textit{Java}.
\end{itemize}

\subsection{Dane tekstowe i binarne}
\textit {Hadoop} pozwala operować zarówno na plikach tekstowych jak \textit{CSV} (ang. \textit{Comma Seperated Values}), \textit{JSON}, \textit{XML} jak i~również binarnych. Istnieje również możliwość definiowania własnych formatów.

\section{Zarządzanie zadaniami i zasobami w klastrze}
\label{yarn}
\textit{YARN} (ang. \textit{Yet Another Resource Negotiator}) jest systemem do zarządzania zasobami w~klastrze. Pojawił się w~drugiej wersji projektu, a~ideą utworzenia była chęć optymalizacji, jak również uniezależnienia zarządzania zasobami (\textit{YARN}) od zarządzania wykonywaniem aplikacji \textit{Map/Reduce}. Aplikacja koordynująca wykonanie poszczególnych faz \textit{map} i~\textit{reduce} stała się klientem \textit{YARN}, który stał się całkowicie niezależny od modelu \textit{Map/Reduce}. Otworzyło to platformę na powstawanie zupełnie nowych aplikacji realizujących nowe modele przetwarzania, jak \textit{Spark}.

\subsubsection*{Zarządzanie zasobami}
Na rysunku \ref{fig:yarn} przedstawiono schemat działania \textit{YARN}. Istnieje jeden proces \textit{Resource Manager} odpowiedzialny za koordynację całości. Klaster składa się z~węzłów, z~których każdy posiada pewne zasoby jak: pamięć, procesor, dysk, zasoby sieciowe i~inne. Na każdym z~węzłów uruchomiony jest serwer \textit{NodeManager} odpowiadający za zgłaszanie do \textit{Resource Manager'a} statusu z~informacją o~wolnych zasobach oraz za uruchamianie na żądanie kontenerów, na których uruchamiane są aplikacje użytkowników. Przy czym poszczególne kontenery uruchamiane są w izolacji (awaria kontenera nie powoduje awarii innych).
\begin{figure}[h]
\centering
\includegraphics[width=16cm]{yarn}
\caption{Schemat działania \textit{YARN} \cite{Hadoop}}
\label{fig:yarn}
\end{figure}                    

\subsubsection*{Zarządzanie aplikacjami}
Klient (program inicjujący aplikację rozproszoną) rezerwuje w~\textit{YARN} zasoby na uruchomienie aplikacji koordynującej przetwarzanie rozproszone (\textit{Application Master}). Po zainicjowaniu i~uruchomieniu, uruchamia ona i~koordynuje kolejne zadania wykonywane na klastrze (rezerwując zasoby również za pośrednictwem \textit{YARN}). 

Na klastrze może działać jednocześnie wiele aplikacji rozproszonych, z~których każda może uruchamiać wiele zadań, a~wszystko działa w~izolacji. Dodatkowo istnieje możliwość ponawiania zadań zakończonych błędem przetwarzania.

\subsubsection*{\textit{Map/Reduce}}
\textit{Hadoop} dostarcza gotowej aplikacji koordynującej wykonanie przetwarzania \textit{Map/Reduce}, która może być zgłoszona i~zainicjowana poprzez interfejs \textit{Java}.

\subsubsection*{Kolejkowanie aplikacji}
\textit{Resource Manager} kolejkuje zgłoszenia udostępniania zasobów klastra, przy czym wyróżnić można trzy strategie zarządzania: 
\begin{itemize}
 \item \textit{FIFO} (ang. \textit{First In First Out}) -- zasoby przydzielane są wedle zgłoszenia, kolejna aplikacja czeka na zakończenie poprzedniej;
 \item \textit{Capacity} -- rezerwowane są pule zasobów, dzięki czemu mniejsze aplikacje mają szanse rozpocząć się przed zakończeniem większych;
 \item \textit{Fair} -- zasoby rozdzielane są po równo między skolejkowane aplikacje.
\end{itemize}

\section{Hadoop MR}
Model ten zakłada przetwarzanie zadań za pomocą algorytmu \textit{Map/Reduce}. Algorytm polega na dekompozycji problemu na wiele niezależnych zadań (ang. \textit{task}), które mogą być przetwarzane niezależnie w~dwóch fazach: \textit{map} i~\textit{reduce}. Taki algorytm bardzo łatwo skaluje się poziomo dzięki możliwości ich wykonywania równolegle w~środowisku rozproszonym. Zadania \textit{map} wysyłane są do węzłów z~danymi (zamiast przesyłania dużych ilości danych przez sieć). Dopiero ich wyjście, które w~wielu przypadkach może mieć już znacznie mniejszy rozmiar, przesyłane jest przez sieć do zadań typu \textit{reduce}, gdzie dane zostają agregowane. Ogólny schemat działania przedstawiony jest na rysunku \ref{fig:mr}.Wiele problemów da się zrealizować za pomocą kilka takich wywołań algorytmu {Map/Reduce}, w pozycji \cite{TextProcessing} można znaleźć kilka ciekawych przykładów.

\begin{figure}[h]
\centering
\includegraphics[width=16cm]{mr}
\caption{Przykład działania \textit{Map/Reduce} \cite{Hadoop}}
\label{fig:mr}
\end{figure} 

Elementarną jednostką przetwarzania jest \textit{job}, który zgłaszany jest do \textit{YARN} za pomocą programu zwanego \textit{driver}. Wykonywany jest on w dwóch fazach: \textit{map} oraz opcjonalnym \textit{reduce}. Każda faza realizowana jest za pomocą jednego lub więcej wspomnianych wcześniej zadań (ang. \textit{task}), które realizowane są na różnych węzłach.

Operacja \textit{map} przeprowadzana jest w sposób rozproszony, na każdym węźle, na którym znajduje się porcja danych. W~ten sposób realizowana jest zasada lokalności danych: przez sieć przesyłane są nie dane, a~program, który ma dostęp lokalny do danych. Jednocześnie proces ten może przebiegać równolegle, gdyż każdy wiersz przetwarzany jest niezależnie.

Operacja \textit{map} pełni jeszcze jedną ważną rolę: pozwala dla każdego rekordu wyznaczyć klucz, za pomocą którego nastąpi scalanie z~innymi rekordami. Przetworzone rekordy są sortowane i~uporządkowane między węzłami w~taki sposób, że rekordy o~tym samym kluczu zawsze znajdą się na tym samym węźle. Proces ten nazywa się \textit{shuffling} i~ze względu na konieczność przesyłania danych przez sieć jest bardzo kosztowny czasowo. 

Operacja \textit{reduce} polega na scaleniu i~przetworzeniu rekordów o~tych samych kluczach. Zbiór rekordów o~tym samym kluczu zawsze będzie przetwarzany na jednym węźle.

\section{\textit{SQL} w \textit{Big Data}}
W miarę rozwoju i rozpowszechniania platformy \textit{Hadoop}, pojawiły się projekty pozwa\-lające na dostęp do danych (odczyt) tak jakby były to dane zapisane w relacyjnej bazie danych. Wynika to z~tego, że język \textit{SQL} jest stosunkowo prosty, czytelny i~posiada dużą ekspresję pozwalającą w~zwięzły sposób operować na danych. Dodatkowo jest świetnie zaadoptowany przez przemysł, który przez wiele dekad zdominowany był przez relacyjne bazy danych.

W odróżnieniu jednak od relacyjnych baz danych nie dokonuje się przekształceń i~formatowania danych podczas wprowadzania do systemu (ang. \textit{schema on write}) --- dane \textit{Big Data} mogą pochodzić z~różnych źródeł, mieć różne formaty i~duże wolumeny. Zamiast tego pozwala się na zdefiniowanie schematu relacji do istniejących danych (ang. \textit{schema on read}) \cite{HadoopGuide}.

Projekt \textit{Hive} jako pierwszy umożliwił odpytywanie się danych poprzez język \textit{SQL}. Do zapisu informacji o~źródłach danych i~ich schematach służy serwer \textit{metastore}. Te schematy definiują strukturę danych umożliwiając odczyt ich, jakby to były rekordy posiadające ustalone pola o~określonych typach.

Zapytania \textit{SQL} przesyłane są do komponentu nazywanego \textit{driver}, który tworzy sesję i~koordynuje pracę realizacji zapytania. Wykorzystuje on komponent \textit{compiler} do kompilacji zapytania a~następnie zleca silnikowi (komponent \textit{Engine}) optymalizację i~wykonanie. Zapytanie jest realizowane w modelu \textit{Map/Reduce}, tzn. jest tłumaczone na serię zadań (ang. \textit{job}) wykonywanych w~platformie \textit{Hadoop}.

Takie podejście nie ogranicza elastyczności w~sposobie uzyskiwania i~przechowywania danych w~klastrze, udostępniając jednocześnie dostęp do nich poprzez dobrze znany i~doskonale do tego nadający się język \textit{SQL}. 

\chapter{Spark}
\label{spark}

Projekt \textit{Spark} powstał później niż \textit{Hadoop}, założony w~2009 roku na uniwersytecie UC Berkeley, źródła udostępniono w~2010, a~w~2013 roku trafił do \textit{Apache Software Foundation} \cite{SparkHome}. Obecnie cieszy się dużą i~stale rosnącą popularnością. 

\section{Podstawowe abstrakcje}
Model przetwarzania w \textit{Spark} opiera się na abstrakcji \textit{RDD} (ang. {\textit{Resilient Distributed Datasets}), czyli kolekcji danych o~właściwościach \cite{LearningSpark}:
\begin{itemize}
  \item rozproszona -- podzielona jest na partycje (ang. \textit{partition}), z~których każda może być przechowywana na innym węźle;
  \item niemodyfikowalna -- nie można zmieniać wartości partycji, ale można na jej podstawie obliczyć nową kolekcję;
  \item niezawodna -- wysoki poziom niezawodności osiąga się poprzez możliwość ponownego wyliczenia;
  \item może być przetwarzana równolegle -- każda z partycji może być przetwarzana niezależnie.
\end{itemize}

\medskip
Kolekcja \textit{RDD} może być utworzona z \cite{SparkHome}:
\begin{itemize}
 \item pliku pochodzącego z: lokalnego systemu plików, \textit{HDFS}, wspierane są również liczne inne systemy plików, m.in.: \textit{Casandra, HBase, Amazon S3};
 \item kolekcji przechowywanej lokalnie na jednej maszynie;
 \item bazy danych (\textit{Spark SQL}).
\end{itemize}

\medskip
Można wyróżnić dwa rodzaje \textit{RDD}: 
\begin{itemize}
  \item zwykłe -- przechowujące pewne obiekty o dowolnej, nieznanej strukturze;
  \item \textit{PairRDD} -- kolekcje przechowujące pary klucz-wartość (przy czym klucz i~wartość są dowolnych typów). Te drugie służą do operacji złączania (ang. \textit{join}), agregacji itp.
\end{itemize}

\medskip
Abstrakcja \textit{RDD} w programie \textit{Spark} reprezentowana jest w sposób obiektowy dając logicznie spójny interfejs do operowania na nim. Dzięki temu można uzyskać dużą czytelność i~zwięzłość zapisu programu.

\section{Model przetwarzania}
Model przetwarzania \textit{Spark RDD} udostępnia znacznie więcej operacji niż starszy model \textit{Map/Reduce}. Istnieją dwa rodzaje operacji: 
\begin{itemize}
 \item transformacje -- nakładające się na kolekcje przekształcenia, jakie maja być dokonane w~trakcie obliczeń; operacje te zwracają nową kolekcję \textit{RDD} i~wykonywanie są leniwie (ang. \textit{lazy evaluation}), tzn. dopiero wtedy, gdy wymagane jest obliczenie kolekcji;
 \item akcje -- powodują rozpoczęcie obliczeń kolekcji \textit{RDD}.
\end{itemize}

Podczas nakładania kolejnych transformacji na kolekcje \textit{RDD}, za każdym razem tworzony jest nowy obiekt \textit{RDD} zawierający opis tych transformacji oraz~referencje do kolekcji \textit{RDD}, na podstawie których był utworzony (,,rodzice''). W ten sposób tworzony jest skierowany, acykliczny graf (ang. \textit{DAG -- Directed Acyclic Graph}) zawierający informacje o kolejnych etapach (ang. \textit{stage}), jakie mają być wykonane w~celu wyliczenia poszczególnych kolekcji. I~tak budowane są coraz bardziej skomplikowane informacje o~przekształceniach, ale nie są one wykonywane.

W momencie, gdy kolejnym krokiem jest akcja na wybranej kolekcji \textit{RDD} (np. zapisanie kolekcji w~systemie plików \textit{HDFS}), następuje obliczanie jej rodziców, czyli rekurencyjne wywołanie obliczeń poszczególnych \textit{RDD}. Każda taka faza obliczeń (ang. \textit{stage}) dzielona jest na zadania (ang. \textit{task}), a~każde zadanie to obliczanie pojedynczej partycji danych w~systemie rozproszonym.

Taki model umożliwia budowę znacznie bardziej skomplikowanych operacji niż zakładał pierwotnie \textit{Hadoop Map/Reduce}, lepszą opty\-malizację, możliwość zapamiętywania w~pamięci \textit{RAM} fragmentów danych i ich wykorzystanie bez konieczności ponownego obliczania. Można te fragmenty również zapisywać na dysku nie tracąc możliwości przetwarzania bardzo dużych zbiorów danych.

\section{Programowanie \textit{Spark}}
Podobnie jak w \textit{Hadoop}, istnieje program główny nazywany po ang. \textit{driver}, który definiuje jakie dane będą przetwarzane, ich źródło oraz sposób przetwarzania. Program ten może być pisany w~jednym z~języków: \textit{Python, Scala} lub \textit{Java}. W każdym z nich \textit{RDD} jest reprezentowany jako obiekt, na którym można wykonywać pewne operacje. 

Najczęściej jako parametry tych operacji przekazywane są funkcje, które definiują w~jaki sposób przekształcić elementy kolekcji. W~trakcie wykonywania programu, funkcje te przesyłane są przez sieć do węzłów, na których znajdują się dane. Podobnie więc jak w~\textit{Hadoop} wykorzystywana jest technika przesyłania programu zamiast danych.

Bardzo ważne jest odróżnienie kontekstu przetwarzania lokalnego w~programie \textit{driver} od zdalnego na węzłach. Program \textit{driver'a} posiada dostęp do lokalnych zmiennych, do których nie mają dostępu operacje na \textit{RDD}, gdyż wykonywane są na innych maszynach. Jeśli funkcja odwołuje się do takich zmiennych, \textit{Spark} prześle ich wartość poprzez sieć na poszczególne węzły. Z~tego powodu \textit{driver} nie może bezpośrednio pobierać lub modyfikować danych z~kolekcji rozproszonych -- może tego dokonywać poprzez dostępne na nich operacje.

W modelu występują również bardziej zaawansowane funkcjonalności jak \textit{broadcast}, \textit{accumulator}, własne \textit{partycjonery} i~inne umożliwiającej daleko posuniętą optymalizację \cite{LearningSpark}.

\section{Struktura projektu}
Rysunek \ref{fig:spark} przedstawia komponenty wchodzące w~skład projektu:
\begin{itemize}
  \item \textit{Standalone Scheduler} -- zarządzanie zadaniami podczas wykonywania lokalnego (ułatwia testowanie);
  \item \textit{Sprak Core} -- podstawowe komponenty oraz obsługa \textit{RDD}, czyli niestrukturalnych rozproszonych kolekcji;
  \item \textit{Spark SQL} -- obsługa strukturalnych rozproszonych kolekcji (\textit{Data Frame});
  \item \textit{Spark Streaming} -- przetwarzanie zbliżone do czasu rzeczywistego);
  \item \textit{Spark MLib} -- wsparcie dla \textit{machine learning};
  \item \textit{Spark GraphX} -- biblioteka do operowania na grafach.
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=16cm]{spark}
\caption{Schemat działania \textit{YARN} \cite{LearningSpark}}
\label{fig:spark}
\end{figure}                    

\section{\textit{Spark SQL} i \textit{Data Frame}}
W miarę rozwoju do projektu wprowadzono obsługę zapytań \textit{SQL} umożliwiając operowanie na danych jak na bazach relacyjnych. W~ten sposób wprowadzono \textit{SchemaRDD}, które w odróżnieniu od zwykłych \textit{RDD} pozwalały operować na danych posiadających strukturę. Rozwinięciem tej koncepcji jest \textit{Spark SQL}, który korzysta z~abstrakcji \textit{Data Frame} wzorowanej na bibliotece \textit{Data Frame} z~języka \textit{R}.

\textit{Data Frame} jest to rozproszona kolekcja danych posiadających strukturę, natomiast do jej przetwarzania wykorzystywany jest \textit{RDD}. Dlatego w programowaniu możliwe jest płynne przejście między tymi abstrakcjami \cite{LearningSpark}. Największą korzyścią operowania na tej kolekcji jest możliwość automatycznej optymalizacji zapytań poprzez wbudowany optymalizator \textit{Catalyst Optmizer}. Znajomość struktury danych pozwala na budowanie planu logicznego, fizycznego oraz ich optymalizację \cite{SparkCookbook}.

